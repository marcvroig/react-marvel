import React from 'react'
import { ListOfChars } from './components/ListOfChars'
import { Search } from './components/Search'

export const App = () => (
    <div className='container'>
        <div className='row'>
            <Search />
            <ListOfChars />
        </div>
    </div>
)
