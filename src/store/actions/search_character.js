import '@babel/polyfill'
import axios from 'axios'
import { GET_DATA, GET_DATA_ERROR, GET_DATA_SUCCESS } from './types'

const API =
    'http://gateway.marvel.com/v1/public/characters?ts=1&hash=bdd6e3c100d355f1fe0298e0fc396ccd&apikey=71cf96a3aa7ed3e0d9e18c93f1a93e6a'

export const searchCharacter = char => async dispatch => {
    dispatch(NewSearchChar())
    await axios
        .get(`${API}&name=${char}`)
        .then(response => {
            dispatch(SuccessSearchChar(response.data.data.results))
            if (response.data.data.results.length === 0) {
                const error = 'no results'
                dispatch(ErrorSearchChar(error))
            }
        })
        .catch(error => dispatch(ErrorSearchChar(error)))
}

export const NewSearchChar = () => {
    return {
        type: GET_DATA,
    }
}

export const SuccessSearchChar = char => {
    return {
        type: GET_DATA_SUCCESS,
        payload: char,
    }
}

export const ErrorSearchChar = error => {
    return {
        type: GET_DATA_ERROR,
        payload: 'no results',
    }
}
