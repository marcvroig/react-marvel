import { GET_DATA, GET_DATA_ERROR, GET_DATA_SUCCESS } from '../actions/types'

const initialState = {
    chars: [],
    error: null,
    loading: false,
    char: {},
}

export default function(state = initialState, action) {
    switch (action.type) {
        case GET_DATA:
            return {
                ...state,
                error: null,
                loading: true,
            }
        case GET_DATA_SUCCESS:
            return {
                ...state,
                error: null,
                loading: false,
                chars: action.payload,
            }
        case GET_DATA_ERROR:
            return {
                ...state,
                error: action.payload,
                loading: false,
            }
        default:
            return state
    }
}
