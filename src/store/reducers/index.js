import { combineReducers } from "redux";
import CharReducer from './data_reducer'

export default combineReducers({
  character: CharReducer
})