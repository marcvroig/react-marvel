import styled from 'styled-components'

export const ImgCard = styled.img`
    height: 34vw;
    object-fit: cover;
`
export const DivCard = styled.div`
    :hover{
        box-shadow: 5px 5px 10px #aaaaaa;
    }
`
export const BtnReadMore = styled.a`
    :hover {
        background-color: #fff;
        border: 2px solid #444444;
        color:#444444;
    }
`
