import React from 'react'
import { ImgCard, DivCard, BtnReadMore } from './styles'

const MAX_LENGTH = 100

export const Character = ({ character }) => {
    const { name, thumbnail, description, urls } = character
    const { url } = urls[0]

    return (
        <div className='col-sm-12 col-md-6 col-lg-4 mb-4'>
            <DivCard className='card h-100'>
                <ImgCard
                    src={`${thumbnail.path}.${thumbnail.extension}`}
                    className='card-img-top img-fluid'
                    alt={name}
                />
                <div className='card-body'>
                    <p>{name}</p>
                    <p>{description.substring(0, MAX_LENGTH)}</p>
                    <BtnReadMore href={url} target='_blank' className='btn btn-secondary float-right'>
                        Read more
                    </BtnReadMore>
                </div>
            </DivCard>
        </div>
    )
}
