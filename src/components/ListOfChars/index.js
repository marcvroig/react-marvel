import React, { useEffect } from 'react'
import { Character } from './Character'
import { useDispatch, useSelector } from 'react-redux'
import { searchCharacter } from '../../store/actions/'
import { Ring } from 'react-awesome-spinners'

export const ListOfChars = () => {
    const dispatch = useDispatch()

    useEffect(() => {
        const loadChars = () => dispatch(searchCharacter('spider-man'))
        loadChars()
    }, [dispatch])

    const characters = useSelector(state => state.character)
    const error = useSelector(state => state.character.error)
    const loading = useSelector(state => state.character.loading)
    const { chars } = characters

    return (
        <div className='row mt-5 p-2'>
            {error ? (
                <div className='font-weight-bold text-center mt-4'>
                    no results :(
                </div>
            ) : null}
            {chars.map(char => (
                <Character key={char.id} character={char} />
            ))}
            {loading ? (
                <Ring className='mx-auto img-fluid' color='#a00000' />
            ) : null}
        </div>
    )
}
