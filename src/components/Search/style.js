import styled from 'styled-components'

export const InputSearch = styled.input`
    width: 30em;
    max-width: 100%;
    height: 56px;
`
export const SearchButton = styled.div`
    width: 56px;
    height: 56px;
    background: #EB2328;
    border-radius: 0px 5px 5px 0px;
    text-align: center;
    align-content: center;
    padding: 0.9em;
    cursor: pointer;
`
