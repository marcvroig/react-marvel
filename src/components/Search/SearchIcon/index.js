import * as React from 'react'

export const SearchIcon = props => {
    return (
        <svg width={24} height={24} viewBox='0 0 24 24' {...props}>
            <path d='M17.157 15.11h-1.081l-.383-.374a8.923 8.923 0 10-.957.957l.373.383v1.081L21.962 24 24 21.962l-6.843-6.853zm-8.23 0A6.173 6.173 0 1115.1 8.936a6.163 6.163 0 01-6.173 6.172z' />
        </svg>
    )
}
