import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { searchCharacter } from '../../store/actions/'
import { SearchIcon } from './SearchIcon'
import { InputSearch, SearchButton } from './style'

export const Search = () => {
    const [searchValue, setSearchValue] = useState('')

    const dispatch = useDispatch()

    const resetSearchValue = () => {
        setSearchValue('')
    }
    const handleSearch = e => {
        setSearchValue(e.target.value)
    }
    const handleSubmit = e => {
        e.preventDefault()
        dispatch(searchCharacter(searchValue))
        resetSearchValue()
    }

    return (
        <div className='col-12'>
            <h1>Search your character</h1>
            <form onSubmit={handleSubmit} className='d-flex'>
                <InputSearch
                    type='text'
                    value={searchValue}
                    onChange={handleSearch}
                    name='search'
                    onSubmit={handleSubmit}
                    placeholder='Search Character'
                />
                <SearchButton onClick={handleSubmit}>
                    <SearchIcon fill='#fff' />
                </SearchButton>
            </form>
        </div>
    )
}
