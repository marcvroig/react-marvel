
## Installation

```bash
npm install 
```

## About
I'm using React and Redux hooks with Redux Thunk.

Webpack config from scratch.

Styled components and Axios to fetch data